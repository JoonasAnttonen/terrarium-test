﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using System.Web.Script.Serialization;

namespace Terrarium_Test
{
    struct TerrariumValue
    {
        public int Value { get; set; }
        public int Low { get; set; }
        public int High { get; set; }
    }

    struct TerrariumLights
    {
        public bool On { get; set; }
        public int StartOfDay { get; set; }
        public int EndOfDay { get; set; }
    }

    struct TerrariumPump
    {
        public int ActivatedOn { get; set; }
        public int ActivatedWait { get; set; }
        public int ActiveFor { get; set; }
    }

    struct TerrariumParameter
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public TerrariumParameter( string key, string value )
            : this()
        {
            Key = key;
            Value = value;
        }
    }

    struct TerrariumHistoryEntry
    {
        public String Timestamp { get; set; }
        public uint Temperature { get; set; }
        public uint Humidity { get; set; }
    }

    class TerrariumHistory
    {
        public List<TerrariumHistoryEntry> Entries { get; set; }

        public TerrariumHistory()
        {
            Entries = new List<TerrariumHistoryEntry>();
        }
    }

    class TerrariumRequest
    {
        public List<TerrariumParameter> Parameters = new List<TerrariumParameter>();

        JavaScriptSerializer Serializer = new JavaScriptSerializer();

        public TerrariumRequest()
        {
        }

        public T DeserializeResponse<T>( string response )
        {
            return Serializer.Deserialize<T>( response );
        }

        static byte[] BuildQuery( List<TerrariumParameter> parameters )
        {
            var builder = new StringBuilder();

            // Build the query url string.
            foreach ( var parameter in parameters )
                builder.AppendFormat( "{0}={1}&", Uri.EscapeDataString( parameter.Key ), Uri.EscapeDataString( parameter.Value ) );

            // Remove the trailing "&".
            builder.Length -= 1;

            // Convert the query to a byte array.
            return Encoding.UTF8.GetBytes( builder.ToString() );
        }

        public bool Execute( string address, out string response )
        {
            var requestData = BuildQuery( Parameters );
            var request = WebRequest.Create( address ) as HttpWebRequest;
            request.ServicePoint.Expect100Continue = false;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = requestData.Length;
            request.UserAgent = "Terrarium Test";
            request.Method = "POST";
            //request.Headers[ "Accept-Charset" ] = "utf-8";
            request.KeepAlive = false;

            // Write our request data.
            using ( var writeStream = request.GetRequestStream() )
            {
                writeStream.Write( requestData, 0, requestData.Length );
                writeStream.Flush();
            }

            // Get the response.
            HttpWebResponse webresponse;

            bool ok = false;

            try
            {
                webresponse = (HttpWebResponse)request.GetResponse();
                ok = true;
            }
            catch ( WebException e )
            {
                webresponse = (HttpWebResponse)e.Response;
                ok = false;
            }

            if ( webresponse != null )
            {
                using ( var reader = new System.IO.StreamReader( webresponse.GetResponseStream() ) )
                {
                    response = reader.ReadToEnd();
                }
            }
            else
            {
                response = String.Empty;
            }

            return ok;
        }
    }

    class TerrariumError
    {
        public int Error { get; set; }
        public string Message { get; set; }
    }

    struct TerrariumState
    {
        public string Name { get; set; }
        public TerrariumValue Temperature { get; set; }
        public TerrariumValue Humidity { get; set; }
        public TerrariumLights Lights { get; set; }
        public TerrariumPump Pump { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine( String.Format( "Name = {0}", Name ) );
            builder.AppendLine( String.Format( "Temperature = {0}C", Temperature.Value ) );
            builder.AppendLine( String.Format( "TemperatureThresholdLow = {0}C", Temperature.Low ) );
            builder.AppendLine( String.Format( "TemperatureThresholdHigh = {0}C", Temperature.High ) );
            builder.AppendLine( String.Format( "Humidity = {0}%", Humidity.Value ) );
            builder.AppendLine( String.Format( "HumidityThresholdLow = {0}%", Humidity.Low ) );
            builder.AppendLine( String.Format( "HumidityThresholdHigh = {0}%", Humidity.High ) );
            builder.AppendLine( String.Format( "LightsEnabled = {0}", Lights.On ) );
            builder.AppendLine( String.Format( "LightsBeginOfDay = {0}", Lights.StartOfDay ) );
            builder.AppendLine( String.Format( "LightsEndOfDay = {0}", Lights.EndOfDay ) );
            builder.AppendLine( String.Format( "PumpActivatedOn = {0}", Pump.ActivatedOn ) );
            builder.AppendLine( String.Format( "PumpActivatedWait = {0}", Pump.ActivatedWait ) );
            builder.AppendLine( String.Format( "PumpActiveFor = {0}", Pump.ActiveFor ) );
            return builder.ToString();
        }
    }

    class Terrarium
    {
        public string Address { get; set; }

        TerrariumState state;
        TerrariumHistory history;

        static string TimestampToISO8601String( uint timestamp )
        {
            uint ss = timestamp % 60;
            timestamp /= 60;
            uint mm = timestamp % 60;
            timestamp /= 60;
            uint hh = timestamp;

            return String.Format( "{0:D2}:{1:D2}:{2:D2}", hh, mm, ss );
        }

        public void UpdateState()
        {
            var request = new TerrariumRequest();
            request.Parameters.Add( new TerrariumParameter( "Method", "GetState" ) );

            string response;

            if ( !request.Execute( Address, out response ) )
            {
                var terrariumError = request.DeserializeResponse<TerrariumError>( response );
                Console.WriteLine();
                Console.WriteLine( "FAILED: {0}", terrariumError.Message );
                Console.WriteLine();
                return;
            }

            state = request.DeserializeResponse<TerrariumState>( response );
        }

        public void UpdateHistory()
        {
            var request = new TerrariumRequest();
            request.Parameters.Add( new TerrariumParameter( "Method", "GetHistory" ) );

            var response = String.Empty;

            if ( !request.Execute( Address, out response ) )
            {
                var terrariumError = request.DeserializeResponse<TerrariumError>( response );
                Console.WriteLine();
                Console.WriteLine( "FAILED: {0}", terrariumError.Message );
                Console.WriteLine();
                return;
            }

            var values = request.DeserializeResponse<List<List<uint>>>( response );
            var newHistory = new TerrariumHistory();

            foreach ( var valueList in values )
            {
                var entry = new TerrariumHistoryEntry();
                entry.Timestamp = TimestampToISO8601String( valueList[ 0 ] );
                entry.Temperature = valueList[ 1 ];
                entry.Humidity = valueList[ 2 ];
                history.Entries.Add( entry );
            }

            history = newHistory;

            Console.WriteLine( "OK" );
        }

        public void SetTemperatureLimits( byte low, byte high )
        {
            var request = new TerrariumRequest();
            request.Parameters.Add( new TerrariumParameter( "Method", "SetTemperatureLimits" ) );
            request.Parameters.Add( new TerrariumParameter( "Low", low.ToString() ) );
            request.Parameters.Add( new TerrariumParameter( "High", high.ToString() ) );

            string response;

            if ( !request.Execute( Address, out response ) )
            {
                var terrariumError = request.DeserializeResponse<TerrariumError>( response );
                Console.WriteLine();
                Console.WriteLine( "FAILED: {0}", terrariumError.Message );
                Console.WriteLine();
            }

            Console.WriteLine( "OK" );
        }

        public void SetHumidityLimits( byte low, byte high )
        {
            var request = new TerrariumRequest();
            request.Parameters.Add( new TerrariumParameter( "Method", "SetHumidityLimits" ) );
            request.Parameters.Add( new TerrariumParameter( "Low", low.ToString() ) );
            request.Parameters.Add( new TerrariumParameter( "High", high.ToString() ) );

            string response;

            if ( !request.Execute( Address, out response ) )
            {
                var terrariumError = request.DeserializeResponse<TerrariumError>( response );
                Console.WriteLine();
                Console.WriteLine( "FAILED: {0}", terrariumError.Message );
                Console.WriteLine();
            }

            Console.WriteLine( "OK" );
        }

        public void SetPumpLimits( uint activeFor, long activatedWait )
        {
            var request = new TerrariumRequest();
            request.Parameters.Add( new TerrariumParameter( "Method", "SetPumpLimits" ) );
            request.Parameters.Add( new TerrariumParameter( "ActiveFor", activeFor.ToString() ) );
            request.Parameters.Add( new TerrariumParameter( "ActivatedWait", activatedWait.ToString() ) );

            string response;

            if ( !request.Execute( Address, out response ) )
            {
                var terrariumError = request.DeserializeResponse<TerrariumError>( response );
                Console.WriteLine();
                Console.WriteLine( "FAILED: {0}", terrariumError.Message );
                Console.WriteLine();
            }

            Console.WriteLine( "OK" );
        }

        public void SetDayLimits( int startOfDay, int endOfDay )
        {
            var request = new TerrariumRequest();
            request.Parameters.Add( new TerrariumParameter( "Method", "SetDayLimits" ) );
            request.Parameters.Add( new TerrariumParameter( "StartOfDay", startOfDay.ToString() ) );
            request.Parameters.Add( new TerrariumParameter( "EndOfDay", endOfDay.ToString() ) );

            string response;

            if ( !request.Execute( Address, out response ) )
            {
                var terrariumError = request.DeserializeResponse<TerrariumError>( response );
                Console.WriteLine();
                Console.WriteLine( "FAILED: {0}", terrariumError.Message );
                Console.WriteLine();
            }

            Console.WriteLine( "OK" );
        }

        public override string ToString()
        {
            return state.ToString();
        }
    }

    class Program
    {
        static void PrintUsage()
        {
            Console.WriteLine( "What would you like to do?" );
            Console.WriteLine( "0 = Print the usage instructions." );
            Console.WriteLine( "1 = Print the Terrarium info to the screen." );
            Console.WriteLine( "2 = Update the Terrarium info and print it to the screen." );
            Console.WriteLine( "3 = Set the temperature limits." );
            Console.WriteLine( "4 = Set the humidity limits." );
            Console.WriteLine( "5 = Set the day limits." );
            Console.WriteLine( "6 = Set the pump limits." );
        }

        static void Main( string[] args )
        {
            var terrarium = new Terrarium();
            terrarium.Address = "http://127.0.0.1/";
            //terraario.Address = "http://192.168.100.15/";
            //terraario.Address = "http://91.152.235.241/";

            Console.WriteLine("Welcome to the Terrarium Test Software!");
            PrintUsage();

            while ( true )
            {
                var line = Console.ReadLine();
                var number = 0;

                if ( !int.TryParse( line, out number ) )
                {
                    Console.WriteLine( "Not a recognized command." );
                }
                else
                {
                    switch ( number )
                    {
                        case 0:
                            PrintUsage();
                            break;
                        case 1:
                            Console.WriteLine();
                            Console.WriteLine( terrarium );
                            break;
                        case 2:
                            Console.WriteLine( "Updating the Terrarium information..." );
                            Console.WriteLine();
                            terrarium.UpdateState();
                            Console.WriteLine( terrarium );
                            break;
                        case 3:
                            DoSetTemperatureLimits( terrarium );
                            break;
                        case 4:
                            DoSetHumidityLimits( terrarium );
                            break;
                        case 5:
                            DoSetDayLimits( terrarium );
                            break;
                        case 6:
                            DoSetPumpLimits( terrarium );
                            break;
                        default:
                            Console.WriteLine( "Not a recognized command." );
                            break;
                    }
                }

                //Console.WriteLine( "Press ENTER to continue..." );
                //Console.ReadLine();
            }
        }

        static void DoSetTemperatureLimits( Terrarium terrarium )
        {
            byte low = 10;
            byte high = 50;

            while ( true )
            {
                Console.WriteLine( "SetTemperatureLimits takes 2 parameters: Low and High." );
                Console.WriteLine( "Input them like this: 25,50" );

                var line = Console.ReadLine();
                var lineSplit = line.Split( ',' );

                if ( !byte.TryParse( lineSplit[ 0 ], out low ) || !byte.TryParse( lineSplit[ 1 ], out high ) )
                {
                    Console.WriteLine( "Invalid numbers! Try Again!" );
                    continue;
                }
                else
                {
                    break;
                }
            }

            Console.WriteLine( "Setting the temperature limits..." );
            terrarium.SetTemperatureLimits( low, high );
        }

        static void DoSetHumidityLimits( Terrarium terrarium )
        {
            byte low = 10;
            byte high = 100;

            while ( true )
            {
                Console.WriteLine( "SetHumidityLimits takes 2 parameters: Low and High." );
                Console.WriteLine( "Input them like this: 25,50" );

                var line = Console.ReadLine();
                var lineSplit = line.Split( ',' );

                if ( !byte.TryParse( lineSplit[ 0 ], out low ) || !byte.TryParse( lineSplit[ 1 ], out high ) )
                {
                    Console.WriteLine( "Invalid numbers! Try Again!" );
                    continue;
                }
                else
                {
                    break;
                }
            }

            Console.WriteLine( "Setting the humidity limits..." );
            terrarium.SetHumidityLimits( low, high );
        }


        static void DoSetDayLimits( Terrarium terrarium )
        {
            int startOfDay = 8;
            int endOfDay = 21;

            while ( true )
            {
                Console.WriteLine( "SetDayLimits takes 2 parameters: StartOfDay and EndOfDay." );
                Console.WriteLine( "Input them like this: 25,50" );

                var line = Console.ReadLine();
                var lineSplit = line.Split( ',' );

                if ( !int.TryParse( lineSplit[ 0 ], out startOfDay ) || !int.TryParse( lineSplit[ 1 ], out endOfDay ) )
                {
                    Console.WriteLine( "Invalid numbers! Try Again!" );
                    continue;
                }
                else
                {
                    break;
                }
            }

            Console.WriteLine( "Setting the day limits..." );
            terrarium.SetDayLimits( startOfDay, endOfDay );
        }

        static void DoSetPumpLimits( Terrarium terrarium )
        {
            uint activeFor = 2000;
            long activatedWait = 10;

            while ( true )
            {
                Console.WriteLine( "SetPumpLimits takes 2 parameters: ActiveFor and ActivatedWait." );
                Console.WriteLine( "Input them like this: 25,50" );

                var line = Console.ReadLine();
                var lineSplit = line.Split( ',' );

                if ( !uint.TryParse( lineSplit[ 0 ], out activeFor ) || !long.TryParse( lineSplit[ 1 ], out activatedWait ) )
                {
                    Console.WriteLine( "Invalid numbers! Try Again!" );
                    continue;
                }
                else
                {
                    break;
                }
            }

            Console.WriteLine( "Setting the pump limits..." );
            terrarium.SetPumpLimits( activeFor, activatedWait );
        }
    }
}
